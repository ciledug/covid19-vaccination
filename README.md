## About

This code is for a web-based dashboard application to manage registrants for Covid-19 vaccination in several places in East Java, Indonesia, which was held in September to October 2021.

## Frameworks

The application was built on top of these frameworks and techs:

- [Laravel Jetstream][lj]
- [Vue.js][vue]
- [Tailwind CSS][tail]
- [MySQL][mysql]
- [jQuery][jquery]

## Disclaimer

DO NOT USE THE CODES WITHOUT OWNER'S WRITTEN PERMISSIONS.

[lj]: <https://jetstream.laravel.com>
[vue]: <https://vuejs.org>
[tail]: <https://tailwindcss.com/>
[mysql]: <https://www.mysql.com>
[jquery]: <https://jquery.com/>
