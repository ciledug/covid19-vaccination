<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class VaccineLocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $locations = array(
            array(
                'uuid' => Str::random(16),
                'kode_lokasi' => 'l-1',
                'kabupaten_kota' => 'Kabupaten Tuban',
                'dinkes' => 'Dinkes Kabupaten Tuban',
                'lokasi' => 'PT Solusi Bangun Indonesia Tbk - Pabrik Tuban. Jl Glondonggede Kerek KM 3 Ds. Merkawang Kec. Tambakboyo, Kabupaten Tuban',
                'perusahaan_pendukung' => 'PT Solusi Bangun Indonesia Tbk - Pabrik Tuban - Holcim',
                'pic_lapangan' => 'Nur Lailiyah',
                'dokter_pic' => 'dr. Agus',
                'tim' => 'Dinkes Kabupaten Tuban',
                'kapasitas' => json_encode(array('dosis_1' => 1000, 'dosis_2' => 1000)),
                'total_vaksin' => 2000,
                'waktu_pelaksanaan' => '2021-09-28 00:00:00',
                'created_at' => DB::raw('NOW()'),
                'updated_at' => DB::raw('NOW()'),
            ),
            array(
                'uuid' => Str::random(16),
                'kode_lokasi' => 'l-2',
                'kabupaten_kota' => 'Kabupaten Sampang',
                'dinkes' => 'Dinkes Kabupaten Sampang',
                'lokasi' => 'Pondok pesantren Al Ihksan Jrengoan Omben Sampang',
                'perusahaan_pendukung' => 'HCML (Husky-CNOOC Madura Limited)',
                'pic_lapangan' => 'Lembaga Kesehatan NU / Mamak',
                'dokter_pic' => 'Dokter DIDIK',
                'tim' => 'Dinkes Kabupaten Sampang dan NU Sampang',
                'kapasitas' => json_encode(array('dosis_1' => 1500, 'dosis_2' => 1500)),
                'total_vaksin' => 3000,
                'waktu_pelaksanaan' => '2021-09-30 00:00:00',
                'created_at' => DB::raw('NOW()'),
                'updated_at' => DB::raw('NOW()'),
            ),
            array(
                'uuid' => Str::random(16),
                'kode_lokasi' => 'l-3',
                'kabupaten_kota' => 'Mojokerto',
                'dinkes' => 'Dinas Kesehatan Kabupaten Mojokerto',
                'lokasi' => "Aula Utama Gedung PCNU Kab. Mojokerto. Jl. R.A. Basuni No.9, Sooko, Mojokerto",
                'perusahaan_pendukung' => 'Intiland Tower, Surabaya',
                'pic_lapangan' => 'Yayasan Komunitas Terong Gosong',
                'dokter_pic' => 'Dinkes Mojokerto',
                'tim' => 'Yayasan Terong Gosong dan Tim Dinkes Kab. Mojokerto',
                'kapasitas' => json_encode(array('dosis_1' => 1500, 'dosis_2' => 1500)),
                'total_vaksin' => 3000,
                'waktu_pelaksanaan' => '2021-09-30 00:00:00',
                'created_at' => DB::raw('NOW()'),
                'updated_at' => DB::raw('NOW()'),
            ),
            array(
                'uuid' => Str::random(16),
                'kode_lokasi' => 'l-4',
                'kabupaten_kota' => 'Kabupaten Pasuruan',
                'dinkes' => 'Dinkes Kabupaten Pasuruan',
                'lokasi' => 'Pendopo Kecamatan Kraton, Kabupaten Pasuruan',
                'perusahaan_pendukung' => 'HCML (Husky-CNOOC Madura Limited)',
                'pic_lapangan' => 'Ali Aliyudin',
                'dokter_pic' => 'dr. Wisnu (08111361238)',
                'tim' => 'Klink Swasta GSM',
                'kapasitas' => json_encode(array('dosis_1' => 2500, 'dosis_2' => 2500)),
                'total_vaksin' => 5000,
                'waktu_pelaksanaan' => '2021-09-30 00:00:00',
                'created_at' => DB::raw('NOW()'),
                'updated_at' => DB::raw('NOW()'),
            ),
            array(
                'uuid' => Str::random(16),
                'kode_lokasi' => 'l-5',
                'kabupaten_kota' => 'Kota Surabaya',
                'dinkes' => 'Dinkes Kota Surabaya',
                'lokasi' => 'Intiland Tower Surabaya. Jl. Panglima Sudirman 101-103, Surabaya',
                'perusahaan_pendukung' => 'PT Intiland Development Tbk.',
                'pic_lapangan' => 'Rosyid',
                'dokter_pic' => 'Dinkes Surabaya',
                'tim' => 'Dinkes Kota Surabaya',
                'kapasitas' => json_encode(array('dosis_1' => 1500, 'dosis_2' => 1500)),
                'total_vaksin' => 3000,
                'waktu_pelaksanaan' => '2021-10-02 00:00:00',
                'created_at' => DB::raw('NOW()'),
                'updated_at' => DB::raw('NOW()'),
            ),
            array(
                'uuid' => Str::random(16),
                'kode_lokasi' => 'l-6',
                'kabupaten_kota' => 'Kabupaten Mojokerto',
                'dinkes' => 'Dinkes Kabupaten Mojokerto',
                'lokasi' => 'Ngoro Industrial Estate. Ngoro Industrial Park, Jl. Raya Ngoro, Mojokerto',
                'perusahaan_pendukung' => 'PT Intiland Development Tbk.',
                'pic_lapangan' => 'Wihardi',
                'dokter_pic' => 'Dinkes Mojokerto',
                'tim' => 'Dinkes Kab. Mojokerto dan Tim Intiland',
                'kapasitas' => json_encode(array('dosis_1' => 2000, 'dosis_2' => 2000)),
                'total_vaksin' => 4000,
                'waktu_pelaksanaan' => '2021-10-02 00:00:00',
                'created_at' => DB::raw('NOW()'),
                'updated_at' => DB::raw('NOW()'),
            ),

            /*
            array(
                'uuid' => Str::random(16),
                'kode_lokasi' => 'l-6',
                'kabupaten_kota' => 'Mojokerto',
                'dinkes' => 'Dinas Kesehatan Kabupaten Mojokerto',
                'lokasi' => 'Kecamatan Gedeg',
                'perusahaan_pendukung' => 'Yayasan Kepak Sayapku',
                'pic_lapangan' => 'Anis Boesra',
                'dokter_pic' => 'Dinkes Mojokerto',
                'tim' => 'YKS',
                'kapasitas' => 2000,
                'total_vaksin' => 4000,
                'waktu_pelaksanaan' => json_encode(array('tahun' => 2021, 'bulan' => 9, 'minggu' => 4)),
                'created_at' => DB::raw('NOW()'),
                'updated_at' => DB::raw('NOW()'),
            ),
            array(
                'uuid' => Str::random(16),
                'kode_lokasi' => 'l-7',
                'kabupaten_kota' => 'Pasuruan',
                'dinkes' => 'Dinas Kesehatan Kabupaten Pasuruan',
                'lokasi' => 'Desa Semare, Kabupaten Pasuruan',
                'perusahaan_pendukung' => 'HCML (Husky-CNOOC Madura Limited)',
                'pic_lapangan' => 'Ali Aliyudin',
                'dokter_pic' => 'dr. Wisnu',
                'tim' => 'Klinik Swasta GSM',
                'kapasitas' => 2500,
                'total_vaksin' => 5000,
                'waktu_pelaksanaan' => json_encode(array('tahun' => 2021, 'bulan' => 9, 'minggu' => 4)),
                'created_at' => DB::raw('NOW()'),
                'updated_at' => DB::raw('NOW()'),
            ),
            array(
                'uuid' => Str::random(16),
                'kode_lokasi' => 'l-9',
                'kabupaten_kota' => 'Banyuwangi',
                'dinkes' => 'Dinas Kesehatan Banyuwangi',
                'lokasi' => 'Desa Pesanggaran',
                'perusahaan_pendukung' => 'Yayasan Kepak Sayapku',
                'pic_lapangan' => 'Nofa',
                'dokter_pic' => 'Dinkes Banyuwangi',
                'tim' => 'Dokter perusahaan dan klinik swasta',
                'kapasitas' => 5000,
                'total_vaksin' => 10000,
                'waktu_pelaksanaan' => json_encode(array('tahun' => 2021, 'bulan' => 9, 'minggu' => 4)),
                'created_at' => DB::raw('NOW()'),
                'updated_at' => DB::raw('NOW()'),
            ),
            */
        );
        DB::table('vaccine_locations')
            ->insert($locations);
    }
}
