<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVaccineLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vaccine_locations', function (Blueprint $table) {
            $table->engine = 'MyISAM';

            $table->id();
            $table->string('uuid', 16)->unique();
            $table->string('kode_lokasi', 10)->unique();
            $table->string('kabupaten_kota', 50);
            $table->string('dinkes', 50);
            $table->string('lokasi', 255);
            $table->string('perusahaan_pendukung');
            $table->string('pic_lapangan', 30)->default(null)->nullable();
            $table->string('dokter_pic', 30)->default(null)->nullable();
            $table->string('tim', 50);
            $table->json('kapasitas')->default(json_encode(array()));
            $table->integer('total_vaksin');
            $table->timestamp('waktu_pelaksanaan');
            $table->boolean('is_shown')->default(true)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vaccine_locations');
    }
}
