<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

use App\Http\Controllers\CustomerController;
use App\Http\Controllers\ChatbotProjectController;
use App\Http\Controllers\ChatbotMenuController;
use App\Http\Controllers\PharmacistController;

use App\Http\Controllers\VaccineRegistrationController;
use App\Http\Controllers\VaccineRegistrantController;
use App\Http\Controllers\VaccineScreeningController;
use App\Http\Controllers\VaccineLastCheckController;
use App\Http\Controllers\VaccineStatisticController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return Inertia::render('Welcome', [
    //     'canLogin' => Route::has('login'),
    //     'canRegister' => Route::has('register'),
    //     'laravelVersion' => Application::VERSION,
    //     'phpVersion' => PHP_VERSION,
    // ]);

    return Inertia::render('Auth/Login', [
        // 'status' => Route::has('login'),
        'status' => false,
    ]);
});

// Route::middleware(['session', 'session.errors'])->group(function () {
    Route::get('/vaccine/register', [VaccineRegistrationController::class, 'register'])->name('vaccineRegister');
    Route::get('/vaccine/bulk', [VaccineRegistrationController::class, 'bulk'])->name('vaccineBulk');
    Route::get('/vaccine/bulk/download', [VaccineRegistrationController::class, 'bulkDownload'])->name('vaccineBulkDownload');
    Route::get('/vaccine/company', [VaccineRegistrationController::class, 'company'])->name('vaccineCompany');
// });

// Route::middleware(['auth:sanctum', 'verified'])->group(function() {
Route::middleware(['verified'])->group(function() {
    Route::get('/dashboard', function() {
        return redirect()->route('vaccine-registrant');
    })->name('dashboard');

    /*
    Route::get('/pharmacy', function() {
        return Inertia::render('Pharmacy/Pharmacy');
    })->name('pharmacy');

    Route::get('/customer', [CustomerController::class, 'index'])->name('customer');

    // --- chatbots ...
    Route::get('/chatbot', [ChatbotProjectController::class, 'index'])->name('chatbot');
    Route::get('/chatbot/bots', [ChatbotProjectController::class, 'bots'])->name('listchatbot');
    Route::post('/chatbot/store', [ChatbotProjectController::class, 'store'])->name('storechatbot');
    Route::put('/chatbot/update', [ChatbotProjectController::class, 'update'])->name('updatechatbot');
    Route::delete('/chatbot/delete', [ChatbotProjectController::class, 'delete'])->name('deletechatbot');

    // --- chatbot menus ...
    Route::get('/chatbot/menus', [ChatbotMenuController::class, 'menus'])->name('chatbotmenus');
    Route::post('/chatbot/menus/store', [ChatbotMenuController::class, 'store'])->name('storechatbotmenus');
    Route::put('/chatbot/menus/update', [ChatbotMenuController::class, 'update'])->name('updatechatbotmenus');
    Route::delete('/chatbot/menus/delete', [ChatbotMenuController::class, 'delete'])->name('deletechatbotmenus');
    Route::get('/chatbot/menus/add', [ChatbotMenuController::class, 'add'])->name('addchatbotmenus');

    // --- pharmacists ...
    Route::get('/pharmacist', [PharmacistController::class, 'index'])->name('pharmacist');
    Route::get('/pharmacist/list', [PharmacistController::class, 'pharmacists'])->name('listpharmacist');
    Route::post('/pharmacist/store', [PharmacistController::class, 'store'])->name('storepharmacist');
    Route::put('/pharmacist/update', [PharmacistController::class, 'update'])->name('updatepharmacist');
    Route::delete('/pharmacist/delete', [PharmacistController::class, 'delete'])->name('deletepharmacist');

    Route::get('/store', function() {
        return Inertia::render('Store/Store');
    })->name('store');
    */

    // --- vaccine ...
    Route::get('/vaccine/registrant', [VaccineRegistrantController::class, 'registrant'])->name('vaccine-registrant');
    Route::get('/vaccine/inputregistrant/{location}/{uuid?}', [VaccineRegistrantController::class, 'inputRegistrant'])
        ->name('vaccine-input-registrant');

    Route::get('/vaccine/screening', [VaccineScreeningController::class, 'screening'])->name('vaccine-screening');
    Route::get('/vaccine/inputscreening/{location}/{uuid}', [VaccineScreeningController::class, 'inputScreening'])
        ->name('vaccine-input-screening');

    Route::get('/vaccine/lastcheck', [VaccineLastCheckController::class, 'lastCheck'])->name('vaccine-last-check');
    Route::get('/vaccine/inputlastcheck/{location}/{uuid?}', [VaccineLastCheckController::class, 'inputLastCheck'])
        ->name('vaccine-input-last-check');

    Route::get('/vaccine/statistic', [VaccineStatisticController::class, 'statistic'])->name('vaccine-statistic');
});
