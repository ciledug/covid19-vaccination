<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\ChatbotProjectController;
use App\Http\Controllers\VaccineRegistrationController;
use App\Http\Controllers\VaccineRegistrantController;
use App\Http\Controllers\VaccineScreeningController;
use App\Http\Controllers\VaccineLastCheckController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::middleware(['auth:sanctum', 'verified'])->group(function() {
Route::middleware(['verified'])->group(function() {
    // Route::post('/chatbot/store', [ChatbotProjectController::class, 'store'])->name('storechatbot');

    Route::post('/vaccine/get-registrant-list', [VaccineRegistrantController::class, 'getRegistrantList'])->name('vaccine-get-registrants');
    Route::post('/vaccine/save-registrant', [VaccineRegistrantController::class, 'saveRegistrant'])->name('store-registrant');
    Route::post('/vaccine/save-registrant-bulk', [VaccineRegistrantController::class, 'saveRegistrantBulk'])->name('store-registrant-bulk');

    Route::post('/vaccine/get-screening-list', [VaccineScreeningController::class, 'getScreeningList'])->name('vaccine-get-screenings');
    Route::post('/vaccine/save-screening', [VaccineScreeningController::class, 'saveScreening'])->name('store-screening');

    Route::post('/vaccine/get-last-check-list', [VaccineLastCheckController::class, 'getLastCheckList'])->name('vaccine-get-last-checks');
    Route::post('/vaccine/save-last-check', [VaccineLastCheckController::class, 'saveLastCheck'])->name('store-last-check');

    Route::post('/vaccine/get-statistic', [VaccineStatisticController::class, 'getStatistic'])->name('vaccine-get-statistic');
});

Route::post('/vacccine/do-register', [VaccineRegistrationController::class, 'doRegister'])->name('vaccine-do-register');
Route::post('/vacccine/do-bulk', [VaccineRegistrationController::class, 'doBulk'])->name('vaccine-do-bulk');
Route::post('/vacccine/do-company', [VaccineRegistrationController::class, 'doCompany'])->name('vaccine-do-company');
Route::post('/vacccine/regnumber', [VaccineRegistrantController::class, 'doRegistrationNumber'])->name('vaccine-do-regnumber');
