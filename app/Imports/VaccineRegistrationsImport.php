<?php

namespace App\Imports;

use App\Models\Customer;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;

class VaccineRegistrationsImport implements ToModel
{

    use Importable;

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Customer([
            'ktp_nik' => $row[0],
            'full_name' => $row[1],
            'handphone' => $row[2],
            'date_of_birth' => $row[4],
            'sex' => $row[5],
            'address' => $row[6],
            'kelurahan' => $row[7],
            'kecamatan' => $row[8],
            'kab_kota' => $row[9],
            'province' => $row[10],
        ]);
    }
}
