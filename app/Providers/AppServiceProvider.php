<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Session;
use Inertia\Inertia;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Inertia::share([
            'errors' => function() {
                return Session::get('errors')
                    ? Session::get('errors')->getBag('default')->getMessages()
                    : (object) [];
            }
        ]);

        Inertia::share('flash', function() {
            return [
                'message' => Session::get('message'),
            ];
        });

        Inertia::share('chatbot', function() {
            return [
                'bots' => Session::get('bots'),
                'menus' => Session::get('menus'),
                'pharmacists' => Session::get('pharmacists'),
            ];
        });

        Inertia::share('vaccine', function() {
            return [
                'registrants' => Session::get('registrants'),
                'screenings' => Session::get('screenings'),
                'lastchecks' => Session::get('lastchecks'),
            ];
        });
    }
}
