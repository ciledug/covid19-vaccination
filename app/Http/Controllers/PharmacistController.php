<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Inertia\Inertia;

use App\Models\Pharmacist;

class PharmacistController extends Controller
{
    public function index(Request $request)
    {
        return Inertia::render('Pharmacist/Pharmacist', [
            'pharmacistList' => $this->pharmacists($request, false),
            'isPharmacistListShown' => true,
        ]);
    }

    /**
     * Method for getting pharmacist list from database
     *
     * params Request $request
     * params boolean $isWithRedirect
     */
    public function pharmacists(Request $request, $isWithRedirect=true)
    {
        $pharmacists = Pharmacist::select([
                'full_name', 'wa_number', 'stra_strttk', 'timezone',
                'monday_hours', 'tuesday_hours', 'wednesday_hours', 'thursday_hours', 'friday_hours', 'saturday_hours', 'sunday_hours',
            ])
            ->orderBy('full_name', 'asc')
            // ->limit(15)
            ->get();
        // dd($pharmacists);

        if ($pharmacists) {
            $tempMask = '';

            foreach ($pharmacists AS $key => $value) {
                $pharmacists[$key]['wa_number'] =
                    str_pad(substr($value['wa_number'], 0, strpos($value['wa_number'], '8') + 3), 12, 'x');

                $pharmacists[$key]['employment'] = (rand(0, 1) == 0) ? 'Unemployed' : 'Employed';

                $pharmacists[$key]['schedules'] = array(
                    'monday' => $value['monday_hours'] != null ? $this->getScheduleHour($value['monday_hours']) . ' ' . Str::upper($value['timezone']) : null,
                    'tuesday' => $value['tuesday_hours'] != null ? $this->getScheduleHour($value['tuesday_hours']) . ' ' . Str::upper($value['timezone']) : null,
                    'wednesday' => $value['wednesday_hours'] != null ? $this->getScheduleHour($value['wednesday_hours']) . ' ' . Str::upper($value['timezone']) : null,
                    'thursday' => $value['thursday_hours'] != null ? $this->getScheduleHour($value['thursday_hours']) . ' ' . Str::upper($value['timezone']) : null,
                    'friday' => $value['friday_hours'] != null ? $this->getScheduleHour($value['friday_hours']) . ' ' . Str::upper($value['timezone']) : null,
                    'saturday' => $value['saturday_hours'] != null ? $this->getScheduleHour($value['saturday_hours']) . ' ' . Str::upper($value['timezone']) : null,
                    'sunday' => $value['sunday_hours'] != null ? $this->getScheduleHour($value['sunday_hours']) . ' ' . Str::upper($value['timezone']) : null,
                );

                if ($value['stra_strttk']) {
                    $exploded = explode('/', $value['stra_strttk']);

                    if (count($exploded) == 4) {
                        $tempMask = '';
                        $exploded[0] = str_pad(substr($exploded[0], 0, 4), 8, 'x');

                        for ($i=1; $i<=strlen($exploded[3]); $i++) {
                            $tempMask .= 'x';
                        }
                        $exploded[3] = $tempMask;

                        $pharmacists[$key]['stra_strttk'] =
                            $exploded[0] . '/' . $exploded[1] . '/' . $exploded[2] . '/' . $tempMask;
                    }
                }
            }
        }

        if ($isWithRedirect) {
            return redirect()->back()->with(
                'pharmacists', $pharmacists
            );
        }
        else {
            return $pharmacists;
        }
    }

    private function getScheduleHour($hours)
    {
        $decodeHours = json_decode($hours);
        return $decodeHours[0];
    }
}
