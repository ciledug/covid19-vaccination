<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Inertia\Inertia;

use App\Models\VaccineLocation;
use App\Models\VaccineRegistration;
use App\Models\VaccineLastCheck;

class VaccineLastCheckController extends Controller
{

    private $HEADER_COLUMNS = array('ktp_nik', 'registration_number', 'full_name', 'handphone', 'screening_decision',);
    private $SCREENING_DECISION_TITLE = array('Screening', 'Diberikan vaksin', 'Vaksin ditunda', 'Tidak divaksin');
    private $COLUMN_SORT = array('asc', 'desc');

    public function index()
    {
        return 'There is nothing here. Really.';
    }

    public function lastCheck()
    {
        $locations = array();
        $selLocation = array('address' => 'Pilih Lokasi Vaksinasi');
        $totalData = 0;
        $authUser = Auth::user();
        

        if ($authUser['location'] == null) {
            $locations = VaccineLocation::orderBy('waktu_pelaksanaan', 'asc')->get();
        }
        else {
            $locations = VaccineLocation::where('kode_lokasi', $authUser['location'])->get();
            $totalData = VaccineRegistration::where('scheduled_place', $locations[0]['uuid'])->count();
            $selLocation = $locations[0];
        }

        foreach ($locations AS $key => $value) {
            $theDate = Carbon::parse($value->waktu_pelaksanaan);
            $value->address = 'Tanggal pelaksanaan: ' . $theDate->format('d-m-Y') . "<br><br>";
            $value->address .= $value->kabupaten_kota . "<br>" . Str::replace("\r\n", "<br>", $value->lokasi);
            $locations[$key] = $value;
        }

        return Inertia::render('Vaccine/LastCheck', [
            'locations' => $locations,
            'selLocation' => $selLocation,
            'totalData' => $totalData,
        ]);
    }

    public function getLastCheckList(Request $request)
    {
        // Log::debug($request->input());
        $location = $request->input('location');
        $sqlWhere = array();

        $draw = (int) $request->input('draw');
        $start = (int) $request->input('start');
        $length = (int) $request->input('length');

        $orderBy = $request->input('order');
        $orderByColumn = $this->HEADER_COLUMNS[(int) $orderBy[0]['column']];
        $orderByDir = trim($orderBy[0]['dir']);

        $search = $request->input('search');
        $searchValue = trim($search['value']);

        $totalData = VaccineRegistration::select('id')
            ->where('scheduled_place', $location)
            ->count();

        $sqlWhere = array(
            array('vaccine_registrations.scheduled_place', '=', $location)
        );

        $authUser = Auth::user();
        if ($authUser['location'] != null) {
            $sqlWhere[] = array('vaccine_locations.kode_lokasi', '=', $authUser['location']);
        }

        if (Str::of($searchValue)->isNotEmpty() && (Str::length($searchValue) >= 3)) {
            $sqlWhere[] = array('vaccine_registrations.full_name', 'LIKE', '%' . $searchValue . '%');
        }

        $lastChecks = VaccineLastCheck::select(
                'vaccine_last_checks.uuid AS uuid',
                'vaccine_screenings.age', 'vaccine_screenings.screening_decision', 'vaccine_screenings.notes AS screening_notes',
                'vaccine_registrations.registration_number', 'vaccine_registrations.full_name', 'vaccine_registrations.ktp_nik',
                'vaccine_registrations.handphone',
            )
            ->where($sqlWhere)
            ->whereNull('vaccine_last_checks.date_given')
            ->leftJoin('vaccine_screenings', 'vaccine_last_checks.screening', '=', 'vaccine_screenings.uuid')
            ->leftJoin('vaccine_registrations', 'vaccine_screenings.registration', '=', 'vaccine_registrations.uuid')
            ->leftJoin('vaccine_locations', 'vaccine_registrations.scheduled_place', '=', 'vaccine_locations.uuid')
            ->orderBy($orderByColumn, $orderByDir)
            ->get();
        // dd($lastChecks);

        if ($lastChecks) {
            $dateGiven = Carbon::today()->format('d-m-Y');

            foreach($lastChecks AS $key => $value) {
                $lastChecks[$key]->date_given = $dateGiven;
                $lastChecks[$key]->sex = Str::of($value['sex'])->exactly('m') ? 'Laki-laki' : 'Perempuan';

                $lastChecks[$key]->handphone = Str::replaceFirst('+62', '0', $value['handphone']);
                $lastChecks[$key]->screening_decision_title =
                    $this->SCREENING_DECISION_TITLE[$value['screening_decision']];
            }
        }

        return datatables()->collection($lastChecks)
            ->addColumn('last_check', function($row) {
                // Log::debug($row);
                $screeningStatus = '';
                $theHref = './inputlastcheck/' . $row['scheduled_place'] . '/' . $row['registrant'];
                $actionBtn = '<a type="button" class="btn btn-warning btn-sm px-1 py-1" href="' . $theHref . '">Ubah</a>';

                switch ($row['screening_decision']) {
                    case 1:
                        $screeningStatus = '<span class="alert alert-success alert-sm px-1 py-1" role="alert">--title--</span>';
                        $actionBtn = '';
                        break;
                    case 2:
                        $screeningStatus = '<span class="alert alert-warning alert-sm px-1 py-1" role="alert">--title--</span>';
                        $actionBtn = '';
                        break;
                    case 3:
                        $screeningStatus = '<span class="alert alert-danger alert-sm px-1 py-1" role="alert">--title--</span>';
                        $actionBtn = '';
                        break;
                    case 0:
                    default:
                        break;
                }
                return (
                    Str::replaceFirst('--title--', $row['screening_decision_title'], $screeningStatus) . '' . $actionBtn
                );
            })
            ->rawColumns(['screening_decision'])
            ->make(true);
    }

    public function saveLastCheck(Request $request)
    {
        $lastCheck = $request->input('lastcheck');
        // dd($request->input());

        $returnedData = array(
            'result' => 500,
            'text' => 'Data gagal disimpan',
            'data' => $lastCheck,
        );

        $vaccineLastCheck = VaccineLastCheck::where('uuid', $lastCheck['uuid']);

        if ($vaccineLastCheck) {
            $vaccineLastCheck->update([
                'date_given' => Carbon::now()->toDateTimeString(),
                'dosage' => isset($lastCheck['dosage']) ? $lastCheck['dosage'] :  '',
                'batch_number' => isset($lastCheck['batch']) ? $lastCheck['batch'] : '',
                'notes' => isset($lastCheck['notes']) ? $lastCheck['notes'] : '',
            ]);

            VaccineHistory::where('registrant', $lastCheck['uuid'])->update([
                'end_time' => Carbon::now()->toDateTimeString(),
            ]);

            $returnedData['result'] = 200;
            $returnedData['text'] = 'Data berhasil disimpan';
        }

        return redirect()->back()->with('message', $returnedData);
    }
}
