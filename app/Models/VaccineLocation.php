<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VaccineLocation extends Model
{
    use HasFactory;

    protected $fillable = array(
        'uuid',
        'kode_lokasi',
        'kabupaten_kota',
        'dinkes',
        'lokasi',
        'perusahaan_pendukung',
        'pic_lapangan',
        'dokter_pic',
        'tim',
        'kapasitas',
        'total_vaksin',
        'waktu_pelaksanaan',
        'is_shown',
    );
}
